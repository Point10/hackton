# 10 Pontos

O sistema proposto para ser desenvolvido no evento do "Hackathon" realizado no pelos alunos da Uni-BH, demostrando uma demanda real do COP, bem como a plotagem do plano de fundo do BHMap (rastreamento dos recursos moveis da prefeitura de belo horizonte ) que será entregue aos parceiros AVSystemGEO e Prodabelpara avaliação.

# Descrição do Projeto
O projeto foi desenvolvido pelos alunos Gustavo Santana, Kelvin Jaeder, Kelly Cristiny do curso de Ciencias da computação (7º periiodo) e tem por finalidade apresentar uma aplicação web de ocorrencias como: BHTrans, defesa civil, guarda municipal, SLU, SAMU entre outros com o intuito de garantir a praticidade para o COP analisar essas ocorrencias, possibilitando assim proporcionar suporte com maior agilidade à população. O projeto consiste em uma tela inicial com um campo para o usuario digitar sobre a ocorrencia e escolher se deseja marcar a partir do seu local, ou se deseja solucionar um outro local a partir de um endereço. Depois de marcado no mapa aparecera no telão de monitoramento da COP, quando os analistar identificarem os chamados criados, verificara atravez do mapa as unidades mais proximas para a solução do problema e designa-la para soluciona-la com base em sua localização atual. Um alerta eh emitido para o agente em campo designado para o trabalho e entao o mesmo se encaminha para o local e a finaliza.

# Ferramentas utilizadas 
Para o banco de dados foi utilizado uma ferramenta de software livre phpmyadmin, já para a parte front-end foi utilizado o HTML, CSS, PHP. Também uma API do google maps para a interação da localização de determinada ocorrencia.

![alt text](https://lh3.googleusercontent.com/-UCqhlRAynFE/Wx0Rc_OLcUI/AAAAAAABB64/90CszYNDYkELlLDS3NXWq2aJ0fMA8FejACL0BGAYYCw/h483/unnamed.png)
# Vantagens 
* Torna-se simples e fácil para os usuários informar uma ocorrência em determinado lugar de Belo Horizonte.
* O sistema é gratuito.
* Existe duas opções de localização. A primeira passa a localização automática de onde a pessoa esta com a ocorrencia, já a segunda opção o usuário tem a possibilidade de marcar uma ocorrencia em outro lugar. 